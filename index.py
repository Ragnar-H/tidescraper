from flask import Flask, Response, jsonify, request
from crawler import TideCrawler
app = Flask(__name__)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return Response("<h1>Flask on Now</h1><p>Could not match your path. You visited: /%s</p>" % (path), mimetype="text/html")


@app.route('/ping')
def ping_pong():
    return jsonify({
        "pong": "pong"
    })


@app.route('/tides/<start>/<end>')
def get_tides(start, end):
    days_forecasted, tide_levels = TideCrawler().crawl(start, end)
    return jsonify({
        "days_forecasted": days_forecasted,
        "tide_levels": tide_levels
    })
