import requests
from bs4 import BeautifulSoup
from dateutil.parser import parse

ENDPOINT = "https://www.tide-forecast.com/locations/Akranes/tides/latest"


class TideCrawler():

    def crawl(self, start, end):
        content = self.fetch_content()
        tides_max_min = self.extract_tide_data_points(content)

        first_timestamp = tides_max_min[0]["timestamp"]
        last_timestamp = tides_max_min[-1]["timestamp"]
        timedelta = parse(last_timestamp) - parse(first_timestamp)
        days_forecasted = timedelta.days

        tides_in_range = self.slice_tide_points(
            tides_max_min, start, end
        )

        return [days_forecasted, tides_in_range]

    def fetch_content(self):
        result = requests.get(ENDPOINT)
        return result.content

    def extract_tide_data_points(self, content):
        soup = BeautifulSoup(content, features="html.parser")
        tide_table = soup.find("table", class_="tide-table")
        rows = tide_table.find_all("tr")
        # This table contains sunset/sunrise and tides
        # Lets only focus on tides

        tides_max_min = []
        # There's one date datapoint for multiple tide data points
        current_date = ""
        for row in rows:
            if row.find("th"):
                day_digit_and_month = row.find("th").contents[0].strip()
                date_units = day_digit_and_month.split(" ")
                current_date = date_units[1] + " " + date_units[2]
            if row.find("td", string="Low Tide") or row.find("td", string="High Tide"):
                tides_max_min.append(
                    self.HTML_row_to_object(current_date, row)
                )

        return tides_max_min

    def HTML_row_to_object(self, date, raw_row):
        time = raw_row.find("td", class_="time tide").contents[0].strip()
        timestamp = parse(date + " " + time).isoformat()
        tide_level_mm = float(
            raw_row.find("td", class_="level metric").contents[0].strip().split(" ")[0]) * 1000
        return {
            "timestamp": timestamp,
            "tide_level_mm": tide_level_mm
        }

    def slice_tide_points(self, tide_level_mm, start, end):
        data_points_in_range = []
        index = 0
        while index < len(tide_level_mm) and end >= tide_level_mm[index]["timestamp"]:
            if start <= tide_level_mm[index]["timestamp"] <= end:
                data_points_in_range.append(
                    tide_level_mm[index]
                )

            index += 1

        return data_points_in_range
