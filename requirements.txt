-i https://pypi.org/simple
beautifulsoup4==4.7.1
certifi==2019.3.9
chardet==3.0.4
click==7.0
flask==1.0.3
idna==2.8
itsdangerous==1.1.0
jinja2==2.10.1
markupsafe==1.1.1
python-dateutil==2.8.0
requests==2.22.0
six==1.12.0
soupsieve==1.9.1
urllib3==1.25.3
werkzeug==0.15.4
